import java.util.*;
import java.util.stream.Collectors;

public class WordFrequencyGame {
    public static final String WIHTE_SPACE_REGEX = "\\s+";
    public static final int INITIAL_FREQUENCY = 1;

    public String getResult(String inputStr) {
        String[] wordArr = inputStr.split(WIHTE_SPACE_REGEX);
        if (wordArr.length == INITIAL_FREQUENCY) {
            return inputStr + " " + INITIAL_FREQUENCY;
        }
        try {
            List<Input> inputList = Arrays.stream(wordArr)
                    .map(word -> new Input(word, INITIAL_FREQUENCY))
                    .collect(Collectors.toList());
            inputList = calculateWordFrequency(inputList);
            return inputList.stream().sorted((w1, w2) -> w2.getWordCount() - w1.getWordCount())
                    .map(input -> input.getWord() + " " + input.getWordCount())
                    .collect(Collectors.joining("\n"));
        } catch (Exception e) {
            return "Calculate Error";
        }
}

    private List<Input> calculateWordFrequency(List<Input> inputList) {
        Map<String, Input> wordFrequencyMap = new HashMap<>();
        for (Input input : inputList) {
            String currentWord = input.getWord();
            if (!wordFrequencyMap.containsKey(currentWord)) {
                wordFrequencyMap.put(currentWord, new Input(currentWord, INITIAL_FREQUENCY));
            } else {
                wordFrequencyMap.put(currentWord, new Input(currentWord, input.getWordCount() + 1));
            }
        }
        return new ArrayList<>(wordFrequencyMap.values());
    }


}
